import React from 'react';
import { Link } from 'react-router-dom';

import './ShowsList.scss';

function ShowsList({ data }) {
    return (
        <section className="ShowsList row">
            {data.map(show => (
                <Link 
                    key={show.id} 
                    to={`/shows/${show.id}`}
                    className="ShowsList__item col-12 col-sm-6 col-md-4 col-lg-3"
                >
                    <article>
                        <h2>{show.name}</h2>
                        <img src={show.image.medium} alt={show.name} />
                        <p>Genres: {show.genres.join(', ')}</p>
                    </article>
                </Link>
            ))}
        </section>
        // tytul, jezyk, gatunki, zdjecie, link do strony oficjlanej
    );
}

export {
    ShowsList,
};
