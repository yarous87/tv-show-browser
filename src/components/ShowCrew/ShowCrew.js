import React from 'react';

function ShowCrew({ data }) {
    return (
        <ul>
            {data.map(({ person, type }) => (
                <li key={person.id + type}>
                    {type}: {person.name}
                </li>
            ))}
        </ul>
    )
}

export {
    ShowCrew,
}