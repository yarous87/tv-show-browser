
import React from 'react';

function ShowDetails({ data }) {
    return (
        <article>
            <h2>{data.name}</h2>
            <img src={data.image.medium} alt={data.name} />
            <p>Genres: {data.genres.join(', ')}</p>
            <a 
                href={data.officialSite} 
                target="_blank"
                rel="noopener noreferrer"
            >
                Official Site
            </a>
        </article>
    );
}

export {
    ShowDetails,
};
