import React from 'react';

function ShowEpisodes({ data }) {
    return (
        <ul>
            {data.map(episode => (
                <li key={episode.id}>
                    s{episode.season.toString().padStart(2, '0')}
                    e{episode.number.toString().padStart(2, '0')}:
                    &nbsp;
                    {episode.name}
                </li>
            ))}
        </ul>
    )
}

export {
    ShowEpisodes,
}