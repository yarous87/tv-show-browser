import React, { useState, useEffect } from 'react';
import axios from 'axios';

function Loader({
    url,
    component: Component,
}) {
    // const {
    //     url,
    //     component
    // } = props;

    const [ loading, setLoading ] = useState(true);
    const [ error, setError ] = useState(null);
    const [ data, setData ] = useState([]);

    useEffect(
        () => {
            setLoading(true);

            axios.get(url)
                .then(resp => setData(resp.data))
                .catch(() => setError('Error occured'))
                .finally(() => setLoading(false));
        },
        [url],
    );

    if (loading) {
        return <div>Loading&hellip;</div>
    } else if (error) {
        return <div>{error}</div>
    }

    return <Component data={data} />
}

export {
    Loader,
}