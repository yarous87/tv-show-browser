import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';
import { Home } from '../Home/Home';
import { ShowsListView } from '../../views/ShowsList.view';
import { ShowDetailsView } from '../../views/ShowDetails.view';

function App() {
  return (
    <div className="App container-fluid">
      <Router>
        <>
          <Link to="/">Strona główna</Link>
          <Link to="/shows">Seriale</Link>
        </>

        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/shows" exact component={ShowsListView} />
          <Route path="/shows/:id" component={ShowDetailsView} />
        </Switch>
      </Router>
    </div>
  );
}

export {
  App,
};
