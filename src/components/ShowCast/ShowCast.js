import React from 'react';

function ShowCast({ data }) {
    return (
        <ul>
            {/* data.map(castMember => {
                const person = castMember.person;
                const character = castMember.character;
                return (
                    <li>
                        {person.name} as {character.name}
                    </li>
                )
                rownoznaczny zapisz z tym na dole
            }) */}

            {data.map(({ person, character }) => (
                <li key={person.id}>
                    {person.name} as {character.name}
                </li>
            ))}
        </ul>
    )
}

export {
    ShowCast,
}