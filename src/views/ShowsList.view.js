import React from 'react';
import { Loader } from '../components/Loader/Loader';
import { ShowsList } from '../components/ShowsList/ShowsList';

function ShowsListView() {
    return (
        <Loader
            url="http://api.tvmaze.com/shows"
            component={ShowsList}
        />
    )
}

export {
    ShowsListView,
};
