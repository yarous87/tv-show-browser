import React from 'react';
import { useParams } from 'react-router-dom';
import { Accordion, Card, Button } from 'react-bootstrap';

import { Loader } from '../components/Loader/Loader';
import { ShowDetails } from '../components/ShowDetails/ShowDetails';
import { ShowEpisodes } from '../components/ShowEpisodes/ShowEpisodes';
import { ShowCast } from '../components/ShowCast/ShowCast';
import { ShowCrew } from '../components/ShowCrew/ShowCrew';

function ShowDetailsView() {
    const params = useParams();

    const accordionItems = [
        {
            name: 'Episodes',
            url: `http://api.tvmaze.com/shows/${params.id}/episodes`,
            component: ShowEpisodes
        },
        {
            name: 'Cast',
            url: `http://api.tvmaze.com/shows/${params.id}/cast`,
            component: ShowCast
        },
        {
            name: 'Crew',
            url: `http://api.tvmaze.com/shows/${params.id}/crew`,
            component: ShowCrew
        },
    ];

    return (
        <>
            <Loader
                url={`http://api.tvmaze.com/shows/${params.id}`}
                component={ShowDetails}
            />

            <Accordion>
                {accordionItems.map(
                    ({ name, url, component }, idx) => (
                        <Card>
                            <Card.Header>
                                <Accordion.Toggle as={Button} variant="link" eventKey={idx}>
                                    {name}
                                </Accordion.Toggle>
                            </Card.Header>
                            <Accordion.Collapse eventKey={idx}>
                                <Card.Body>
                                    <Loader
                                        url={url}
                                        component={component}
                                    />
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    )
                )}
            </Accordion>
        </>
    )
}

export {
    ShowDetailsView,
};
